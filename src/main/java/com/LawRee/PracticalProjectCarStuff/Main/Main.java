package com.LawRee.PracticalProjectCarStuff.Main;

import com.LawRee.PracticalProjectCarStuff.Utilities.CarPartsCRUD;
import com.LawRee.PracticalProjectCarStuff.Utilities.PartVendorsCRUD;

public class Main {
    public static void main(String[] args) {
        PartVendorsCRUD vendorsCRUD = new PartVendorsCRUD();
        vendorsCRUD.createVendor();
    }
}
