package com.LawRee.PracticalProjectCarStuff.Entities;

import lombok.Data;
import lombok.experimental.Accessors;
import org.hibernate.annotations.NaturalId;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "partvendors")
@Accessors
@Data
public class PartVendors {
    @Id
    @Column(name ="Id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long Id;
    @NaturalId
    private String name;
    private String CountryOfOrigin;
    private int QualityIndex;

    @OneToMany(mappedBy = "VendorID")
    private Set<CarParts> carParts;


}
