package com.LawRee.PracticalProjectCarStuff.Entities;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Accessors
@Table(name ="CarParts")
@Data
public class CarParts {
    @Id
    @GeneratedValue
    private int Id;
    private String PartName;
    private Long VendorID;
    private int Qnty;
    private long Price;

}
