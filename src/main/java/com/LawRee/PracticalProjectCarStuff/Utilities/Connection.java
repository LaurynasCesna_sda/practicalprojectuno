package com.LawRee.PracticalProjectCarStuff.Utilities;

import com.LawRee.PracticalProjectCarStuff.Entities.CarParts;
import com.LawRee.PracticalProjectCarStuff.Entities.PartVendors;
import org.hibernate.*;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.cfg.Environment;
import org.hibernate.service.ServiceRegistry;

import java.util.Properties;

public class Connection {
    private static SessionFactory sessionFactory;
    public static SessionFactory getSessionFactory(){
     if (sessionFactory == null){
         try {
             Configuration configuration = new Configuration();
             Properties settings = new Properties();
             settings.put(Environment.DRIVER, "com.mysql.jdbc.Driver");
             settings.put(Environment.URL, "jdbc:mysql://localhost:3306/PracticalProjectDatabase?serverTimezone=UTC" );
             settings.put(Environment.USER, "root");
             settings.put(Environment.PASS, "MyNewPass");
             settings.put(Environment.DIALECT, "org.hibernate.dialect.MySQL8Dialect");
             settings.put(Environment.SHOW_SQL, "true");
             settings.put(Environment.CURRENT_SESSION_CONTEXT_CLASS,"thread");
             settings.put(Environment.HBM2DDL_AUTO,"create");
             configuration.setProperties(settings);
             configuration.addAnnotatedClass(CarParts.class);
             configuration.addAnnotatedClass(PartVendors.class);
             ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder()
                     .applySettings(configuration.getProperties()).build();
             sessionFactory = configuration.buildSessionFactory(serviceRegistry);
         } catch(Exception e){
             e.printStackTrace();
         }
    }
     return  sessionFactory;

    }
    public static void shutDown(){
        getSessionFactory().close();
    }
}
