package com.LawRee.PracticalProjectCarStuff.Utilities;

import com.LawRee.PracticalProjectCarStuff.Entities.PartVendors;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.Random;
import java.util.Scanner;

public class PartVendorsCRUD {
    Scanner scan = new Scanner(System.in);
    public void createVendor(){
        PartVendors partVendor = new PartVendors();
        System.out.println("Country of origin");
        partVendor.setCountryOfOrigin(scan.nextLine());
        System.out.println("Name");
        partVendor.setName(scan.nextLine());
        System.out.println("quality index");
        partVendor.setQualityIndex(scan.nextInt());

        Transaction transaction = null;
        try{
            Session session = Connection.getSessionFactory().openSession();
            transaction = session.beginTransaction();
            session.save(partVendor);
            transaction.commit();
        } catch (Exception e){
            e.printStackTrace();
        }
    }

}
